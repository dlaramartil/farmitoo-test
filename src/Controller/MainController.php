<?php

namespace App\Controller;

use App\Entity\Cart;
use App\Entity\Item;
use App\Entity\Product;
use App\Entity\Promotion;
use App\Entity\ShippingRule\ShippingRuleByNbProd;
use App\Entity\ShippingRule\ShippingRulePackage;
use App\Service\CartService;
use App\Service\ShippingService;
use App\Service\VATService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;


class MainController extends AbstractController
{
    public function index(CartService $cartService, VATService $VATService, ShippingService $shippingService): Response
    {
        $product1 = new Product('Cuve à gasoil', 250000, 'Farmitoo');
        $product2 = new Product('Nettoyant pour cuve', 5000, 'Farmitoo');
        $product3 = new Product('Piquet de clôture', 1000, 'Gallagher');

        $promotion1 = new Promotion(50000, 8, false);

        // Je passe une commande avec
        // Cuve à gasoil x1
        $item1 = new Item($product1, 1);
        // Nettoyant pour cuve x3
        $item2 = new Item($product2, 3);
        // Piquet de clôture x5
        $item3 = new Item($product3, 5);

        $cartItems = [$item1, $item2, $item3];
        $cartPromos = [$promotion1];

        $shippingFarmitoo = new ShippingRuleByNbProd("Farmitoo", 20, 3);
        $shippingGallagher = new ShippingRulePackage("Gallagher", 15);
        $shippingService->setShippingByBrand([
            "Farmitoo" => $shippingFarmitoo,
            "Gallagher" => $shippingGallagher
        ]);

        $cart = new Cart($cartItems, $cartPromos);
        $cart = $cartService->computeCartTotals($cart);


        return $this->render('cart.html.twig', ['cart' => $cart]);
    }
}
