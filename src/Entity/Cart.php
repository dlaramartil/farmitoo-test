<?php

namespace App\Entity;

class Cart
{
    /**
     * @var float
     */
    private $totalItemsWoVAT;

    /**
     * @var float
     */
    private $totalPromo;

    /**
     * @var float
     */
    private $totalVAT;

    /**
     * @var float
     */
    private $total;

    /**
     * @var Item[]
     */
    private $items;

    /**
     * @var Promotion[]
     */
    private $promotions;

    /**
     * @var float
     */
    private $deliveryFees;

    /**
     * @var float
     */
    private $totalWoVAT;

    public function __construct(array $items, array $promotions = []){
        $this->promotions = $promotions;
        $this->items = $items;
    }

    /**
     * @return float
     */
    public function getTotalItemsWoVAT(): float
    {
        return $this->totalItemsWoVAT;
    }

    /**
     * @param float $totalItemsWoVAT
     */
    public function setTotalItemsWoVAT(float $totalItemsWoVAT): void
    {
        $this->totalItemsWoVAT = $totalItemsWoVAT;
    }

    /**
     * @return float
     */
    public function getTotalPromo(): float
    {
        return $this->totalPromo;
    }

    /**
     * @param float $totalPromo
     */
    public function setTotalPromo(float $totalPromo): void
    {
        $this->totalPromo = $totalPromo;
    }

    /**
     * @return float
     */
    public function getTotalVAT(): float
    {
        return $this->totalVAT;
    }

    /**
     * @param float $totalVAT
     */
    public function setTotalVAT(float $totalVAT): void
    {
        $this->totalVAT = $totalVAT;
    }

    /**
     * @return float
     */
    public function getTotal(): float
    {
        return $this->total;
    }

    /**
     * @param float $total
     */
    public function setTotal(float $total): void
    {
        $this->total = $total;
    }

    /**
     * @return Item[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param Item[] $items
     */
    public function setItems(array $items): void
    {
        $this->items = $items;
    }

    /**
     * @return float
     */
    public function getDeliveryFees(): float
    {
        return $this->deliveryFees;
    }

    /**
     * @param float $deliveryFees
     */
    public function setDeliveryFees(float $deliveryFees): void
    {
        $this->deliveryFees = $deliveryFees;
    }

    /**
     * @return float
     */
    public function getTotalWoVAT(): float
    {
        return $this->totalWoVAT;
    }

    /**
     * @param float $totalWoVAT
     */
    public function setTotalWoVAT(float $totalWoVAT): void
    {
        $this->totalWoVAT = $totalWoVAT;
    }

    /**
     * @return Promotion[]
     */
    public function getPromotions(): array
    {
        return $this->promotions;
    }

    /**
     * @param Promotion[] $promotions
     */
    public function setPromotions(array $promotions): void
    {
        $this->promotions = $promotions;
    }

}