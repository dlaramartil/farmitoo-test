<?php


namespace App\Entity;


class Item
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * @var int
     */
    protected $quantity;

    public function __construct(Product $product, int $quantity){
        $this->product = $product;
        $this->quantity = $quantity;
    }

    public function getProduct(): Product{
        return $this->product;
    }

    public function getQuantity(): int{
        return $this->quantity;
    }
}
