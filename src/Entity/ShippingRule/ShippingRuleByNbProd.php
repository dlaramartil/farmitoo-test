<?php

namespace App\Entity\ShippingRule;

class ShippingRuleByNbProd extends ShippingRule
{
    protected $nbLotProd;
    protected $feesByLot;

    public function __construct($brand, $fees, $nbLotProd){
        $this->brand = $brand;
        $this->feesByLot = $fees;
        $this->nbLotProd = $nbLotProd;
    }

    public function getShipping(array $items): float{
        $qty = 0;
        foreach ($items as $item){
            $qty += $item->getQuantity();
        }

        $additionalFees = 0;
        if($qty % $this->nbLotProd > 0){
            $additionalFees = 1;
        }

        $nbLot = intdiv($qty, $this->nbLotProd) + $additionalFees;
        return $nbLot * $this->feesByLot;
    }

}