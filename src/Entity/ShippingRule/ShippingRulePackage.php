<?php

namespace App\Entity\ShippingRule;

class ShippingRulePackage extends ShippingRule
{
    protected $fees;

    public function __construct($brand, $fees){
        $this->brand = $brand;
        $this->fees = $fees;
    }

    public function getShipping(array $items): float{
        return $this->fees;
    }

}