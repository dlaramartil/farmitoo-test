<?php

namespace App\Service;
use App\Entity\Cart;
use App\Entity\Item;
use App\Entity\Promotion;

class CartService
{

    protected $vatService;
    protected $shippingService;

    public function __construct(VATService $vatService, ShippingService $shippingService){
        $this->vatService = $vatService;
        $this->shippingService = $shippingService;
    }

    public function computeTotalItems(Cart $cart): float {
        $total = 0;
        foreach ($cart->getItems() as $item){
            $total += $item->getProduct()->getPrice() * $item->getQuantity();
        }

        return $total;
    }

    public function computePromo(Cart $cart): float{
        $total = $this->computeTotalItems($cart);
        $totalPromo = 0;
        foreach ($cart->getPromotions() as $promo) {
            if($total >= $promo->getMinAmount()){
                $totalPromo += round($total * $promo->getReduction() / 100, 2);
            }
        }

        return $totalPromo;
    }

    public function computeShipping(Cart $cart): float{

        foreach ($cart->getPromotions() as $promotion){
            if($promotion->isFreeDelivery()){
                return 0;
            }
        }

        return $this->shippingService->computeShipping($cart);
    }

    public function computeTotalWithoutVAT(Cart $cart): float{
        return $cart->getTotalItemsWoVAT() + $cart->getDeliveryFees() - $cart->getTotalPromo();
    }

    public function computeVAT(Cart $cart): float{
        return $this->vatService->computeVAT($cart->getItems());
    }

    public function computeTotal(Cart $cart): float{
        return $cart->getTotalItemsWoVAT() + $cart->getDeliveryFees() - $cart->getTotalPromo() + $cart->getTotalVAT();
    }

    public function computeCartTotals(Cart $cart): Cart{
        $totalItems = $this->computeTotalItems($cart, false);
        $cart->setTotalItemsWoVAT($totalItems);
        $promo = $this->computePromo($cart);
        $cart->setTotalPromo($promo);
        $shipping = $this->computeShipping($cart);
        $cart->setDeliveryFees($shipping);
        $totalWoVAT = $this->computeTotalWithoutVAT($cart);
        $cart->setTotalWoVAT($totalWoVAT);
        $totalVAT = $this->computeVAT($cart);
        $cart->setTotalVAT($totalVAT);
        $total = $this->computeTotal($cart);
        $cart->setTotal($total);

        return $cart;
    }
}