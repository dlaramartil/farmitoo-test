<?php

namespace App\Service;
use App\Entity\Cart;
use App\Entity\Item;
use App\Entity\Product;

class ShippingService
{
    private $shippingByBrand = [];

    public function setShippingByBrand(array $shippingByBrand){
        $this->shippingByBrand = $shippingByBrand;
    }

    public function computeShipping(Cart $cart): float{
        $shipping = 0;
        $itemsByBrand = [];
        foreach ($cart->getItems() as $item) {
            $itemsByBrand[$item->getProduct()->getBrand()][] = $item;
        }

        foreach (array_keys($itemsByBrand) as $brand){
            $shippingObj = $this->shippingByBrand[$brand];
            $shipping += $shippingObj->getShipping($itemsByBrand[$brand]);
        }

        return $shipping;
    }

}