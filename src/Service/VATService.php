<?php

namespace App\Service;

use App\Entity\Item;
use App\Entity\Product;
use http\Exception;

class VATService{
    private $vatByBrand = [
        "Farmitoo" => 0.2,
        "Gallagher" => 0.05
    ];

    public function getVAT(Product $product){
        if(isset($this->vatByBrand[$product->getBrand()])){
            return $this->vatByBrand[$product->getBrand()];
        } else {
            throw new \Exception("Invalid brand");
        }
    }

    public function computeVAT(array $items): float{
        $vat = 0;
        foreach ($items as $item){
            $vat += $item->getProduct()->getPrice() * $item->getQuantity() * $this->getVAT($item->getProduct());
        }

        return $vat;
    }
}