<?php

namespace App\Tests\Unit\Service;

use App\Entity\Cart;
use App\Entity\Item;
use App\Entity\Product;
use App\Entity\Promotion;
use App\Entity\ShippingRule\ShippingRuleByNbProd;
use App\Service\CartService;
use App\Service\ShippingService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CartServiceTest extends KernelTestCase
{
    public function testComputeShippingWithFreeDeliveryPromotion(){
        self::bootKernel();

        $container = self::$container;

        $product = new Product("my product", 100, "Farmitoo");
        $shippingRule = new ShippingRuleByNbProd("Farmitoo", 10, 5);
        $promo = new Promotion(100, 10, true);

        $item = new Item($product, 10);
        /** @var CartService $cartService */
        $cartService = $container->get(CartService::class);
        /** @var ShippingService $shippingService */
        $shippingService = $container->get(ShippingService::class);
        $shippingService->setShippingByBrand(["Farmitoo" => $shippingRule]);

        $cart = new Cart(
            [$item], [$promo]
        );

        $shipping = $cartService->computeShipping($cart);
        $this->assertEquals(0, $shipping);
    }

    public function testComputeShippingWithoutFreeDeliveryPromotion(){
        self::bootKernel();

        $container = self::$container;

        $product = new Product("my product", 100, "Farmitoo");
        $shippingRule = new ShippingRuleByNbProd("Farmitoo", 10, 5);
        $promo = new Promotion(100, 10, false);

        $item = new Item($product, 10);
        /** @var CartService $cartService */
        $cartService = $container->get(CartService::class);
        /** @var ShippingService $shippingService */
        $shippingService = $container->get(ShippingService::class);

        $cart = new Cart(
            [$item], [$promo]
        );
        $shippingService->setShippingByBrand(["Farmitoo" => $shippingRule]);

        $shipping = $cartService->computeShipping($cart);
        $this->assertEquals(20, $shipping);
    }

    public function testComputePromo(){
        self::bootKernel();

        $container = self::$container;

        $product = new Product("my product", 100, "Farmitoo");
        $shippingRule = new ShippingRuleByNbProd("Farmitoo", 10, 5);
        $promo = new Promotion(1000, 10, false);

        $item = new Item($product, 10);
        $item2 = new Item($product, 9);

        /** @var CartService $cartService */
        $cartService = $container->get(CartService::class);
        /** @var ShippingService $shippingService */
        $shippingService = $container->get(ShippingService::class);

        $cart = new Cart(
            [$item], [$promo]
        );
        $cart2 = new Cart(
            [$item2], [$promo]
        );
        $shippingService->setShippingByBrand(["Farmitoo" => $shippingRule]);

        $promo1 = $cartService->computePromo($cart);
        $promo2 = $cartService->computePromo($cart2);

        $this->assertEquals(100, $promo1);
        $this->assertEquals(0, $promo2);
    }

    public function testComputeTotalItems(){
        self::bootKernel();

        $container = self::$container;

        $product = new Product("my product", 100, "Farmitoo");
        $product2 = new Product("my product deux", 2000, "Gallagher");

        $item = new Item($product, 10);
        $item2 = new Item($product2, 3);

        $cart = new Cart(
            [$item, $item2]
        );

        /** @var CartService $cartService */
        $cartService = $container->get(CartService::class);
        $totalItems = $cartService->computeTotalItems($cart);

        $this->assertEquals(7000, $totalItems);
    }

}