<?php

namespace App\Tests\Unit\Service;

use App\Entity\Cart;
use App\Entity\Item;
use App\Entity\Product;
use App\Entity\Promotion;
use App\Entity\ShippingRule\ShippingRuleByNbProd;
use App\Entity\ShippingRule\ShippingRulePackage;
use App\Service\ShippingService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ShippingServiceTest extends KernelTestCase
{
    public function testComputeShippingByNbProd(){
        self::bootKernel();

        $container = self::$container;

        $product = new Product("my product", 100, "Farmitoo");
        $shipping = new ShippingRuleByNbProd("Farmitoo", 10, 5);

        $item = new Item($product, 10);
        /** @var ShippingService $shippingService */
        $shippingService = $container->get(ShippingService::class);

        $cart = new Cart([$item]);

        $shippingService->setShippingByBrand(["Farmitoo" => $shipping]);
        $shippingFees = $shippingService->computeShipping($cart);
        $this->assertEquals(20, $shippingFees);


        $shipping = new ShippingRuleByNbProd("Farmitoo", 10, 2);
        $shippingService->setShippingByBrand(["Farmitoo" => $shipping]);
        $shippingFees = $shippingService->computeShipping($cart);
        $this->assertEquals(50, $shippingFees);


    }

    public function testComputeShippingByPackage(){
        self::bootKernel();

        $container = self::$container;

        $product = new Product("my product", 100, "Farmitoo");
        $shipping = new ShippingRulePackage("Farmitoo", 10);

        $item = new Item($product, 1);
        /** @var ShippingService $shippingService */
        $shippingService = $container->get(ShippingService::class);

        $cart = new Cart([$item]);

        $shippingService->setShippingByBrand(["Farmitoo" => $shipping]);
        $shippingFees = $shippingService->computeShipping($cart);
        $this->assertEquals(10, $shippingFees);

        $item = new Item($product, 123);
        $cart = new Cart([$item]);
        $shippingFees = $shippingService->computeShipping($cart);
        $this->assertEquals(10, $shippingFees);

    }

}