<?php

namespace App\Tests\Unit\Service;

use App\Entity\Item;
use App\Entity\Product;
use App\Service\VATService;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class VATServiceTest extends KernelTestCase
{
    public function testComputeVAT(){
        self::bootKernel();

        $container = self::$container;

        $product1 = new Product("title test", 100, "Farmitoo");
        $item1 = new Item($product1, 2);

        /** @var VATService $vatService */
        $vatService = $container->get(VATService::class);
        $vat = $vatService->computeVAT([$item1]);
        $this->assertEquals(40, $vat );

        $product2 = new Product("title test deux", 100, "Gallagher");
        $item2 = new Item($product2, 5);
        $vat = $vatService->computeVAT([$item2]);
        $this->assertEquals(25, $vat);

        $vat = $vatService->computeVAT([$item1, $item2]);
        $this->assertEquals(65, $vat);

        $product3 = new Product("title test deux", 100, "brandtest");
        $item3 = new Item($product3, 10);
        $this->expectExceptionMessage("Invalid brand");
        $vatService->computeVAT([$item3]);
    }
}